import * as ts from "typescript";
import * as fs from "fs";

const tsany = ts as any;

interface TypedId {
  id: string,
  type: Object
};

interface DecoratedItem {
  className: string,
  type: string,
  decorates: string,
  decoratorArgs: any[],
  methodParameters: TypedId[]
};

let symtab = {};
let checker;

function tokenObjectToJSON(o:any) {
  let res = null;
  let unknown = false;

  switch(o.kind) {
    case ts.SyntaxKind.StringKeyword: res = { type:"string" }; break;
    case ts.SyntaxKind.NumberKeyword: res = { type:"number" }; break;
    case ts.SyntaxKind.BooleanKeyword: res =  { type:"boolean" }; break;
    case ts.SyntaxKind.AnyKeyword: res = { type:"object" }; break;
    case ts.SyntaxKind.NullKeyword: res = { type:"null" }; break;
    case ts.SyntaxKind.UndefinedKeyword: break;
    case ts.SyntaxKind.SymbolKeyword: break;
    case ts.SyntaxKind.FunctionType: break;
    default: unknown = true; break;
  }
  if(unknown) throw(`unknown type  (${o.kind}) in parameterlist`);
  else return res;
}

function maptypeDescName(name: string): Object {
  if(name == "Object") return { type:"object" };
  if(name == "String") return { type:"string" };
  if(name == "Number") return { type:"number" };
  if(name == "Boolean") return { type:"boolean" };
  if(name == "Function") return null;
  return { "$ref":`#/definitions/${name}` };
}

function unionToJSON(typeDesc:any):Object {
   let unionDesc = <ts.UnionTypeNode>typeDesc;
   let res = { oneOf:[] };

   for(let i = 0;i < unionDesc.types.length;i++) {
     let unionElement = typeToJSON(unionDesc.types[i]);

     if(unionElement != null) res.oneOf.push(unionElement);
   }
   return res;
}

function literalToJSON(typeDesc:any):Object {
  let type = checker.getTypeFromTypeNode(typeDesc);

  if(type.value != null) {
    return { type:(typeof type.value), oneOf:[{ format:type.value }] };
  }
  
  let literal = checker.typeToString(type);

  throw("unknown literal type (" + literal + ")");
}

function typeToJSON(typeDesc:any):Object {
  let res;

  if(typeDesc.constructor.name == 'NodeObject') {
    let unknown = false;

    switch(typeDesc.kind) {
      case ts.SyntaxKind.ArrayType: res =  { type:"array", items:typeToJSON(typeDesc.elementType) }; break;
      case ts.SyntaxKind.TypeReference: res =  maptypeDescName(typeDesc.typeName.text); break;
      case ts.SyntaxKind.FunctionType: break;
      case ts.SyntaxKind.TypeQuery: res = null; break;
      case ts.SyntaxKind.UnionType: res = unionToJSON(typeDesc); break;
      case ts.SyntaxKind.LiteralType: res = literalToJSON(typeDesc); break;
      case ts.SyntaxKind.ParenthesizedType: break;
      //case ts.SyntaxKind.TypeQuery: res = { type:"type query not implemented" }; break;
      //case ts.SyntaxKind.ParenthesizedType: res = { type:"parenthesized type not implemented" }; break;
      default: unknown = true; break;
    }
    if(unknown) throw(`unknown type (${typeDesc.kind}) in parameterlist`); 
  }
  else if(typeDesc.constructor.name == 'TokenObject') res = tokenObjectToJSON(typeDesc);
  else throw(`unknown type (${typeDesc.constructor.name})`);

  if(res) {
    let symbol = checker.getTypeFromTypeNode(typeDesc).symbol;

    if(symbol) res.description = ts.displayPartsToString(symbol.getDocumentationComment());
  }
  return res;
}

function parameterListToJSON(method: DecoratedItem):Object {
  let props = {};
  let parameterNames = [];

  for(let i = 0;i < method.methodParameters.length;i++) {
    let jsonValue = typeToJSON(method.methodParameters[i].type);;

    if(jsonValue) props[method.methodParameters[i].id] = jsonValue;
  }
  for(let i = 0;i < method.methodParameters.length;i++) parameterNames[i] = method.methodParameters[i].id;
  return {
    className: `${method.className}`,
    method: `${method.decorates}`,
    parameterNames:parameterNames,
    schema: {
      "$schema": "http://json-schema.org/draft-06/schema#",
      title: `${method.decorates} plist`,
      description: `Parameter list for ${method.decorates}`,
      type: "object",
      properties: props
    }
  };
}

function traverseParameterList(parms: any): TypedId[] {
  let parameterList:TypedId[] = [];

  for(let i = 0;i < parms.length;i++) {
    parameterList.push(<TypedId>{ id:parms[i].name.text, type:parms[i].type });
  }
  return parameterList;
}

function genSymtabToSchemaDefinitions(): Object {
  let res = {};

  for(let ikey in symtab) {
    let required = [];
    res[ikey] = { type:"object", properties:{} };
    for(let mkey in symtab[ikey].members) {
      res[ikey].properties[mkey] = symtab[ikey].members[mkey].desc;
      if(!symtab[ikey].members[mkey].optional) required.push(mkey);
    }
    res[ikey].required = required;
    if(symtab[ikey].comment != null) res[ikey].description = symtab[ikey].comment;
  }
  return res;
}

function genArgsToSchema(parameterNames: any): string {
  let s = `function(a) {
  let o = {};\n`;
   
  s += "console.log(a);\n";
  for(let i = 0;i < parameterNames.length;i++) {
     s += `    o['${parameterNames[i]}'] = a[${i}];\n`;
  }
  s += "  console.log(o);";
  s += "  return o;\n  }";
  return s;
}

function genMethodEntry(className,methodName,parameterNames,schema) {
  return `
exports["${className}.${methodName}"] = { 
  schema:compositeWithDefinitions(${JSON.stringify(schema)}),
  argsToSchema:${genArgsToSchema(parameterNames)},
  validate:ajv.compile(compositeWithDefinitions(${JSON.stringify(schema)}))
};`;
}

function genSource(items:DecoratedItem[]) {
  let definitions = genSymtabToSchemaDefinitions();

  console.log(`
const Ajv = require('ajv');
let ajv = new Ajv();
let definitions = ${JSON.stringify(definitions,null,2)}

function compositeWithDefinitions(schema) { schema.definitions = definitions; return schema; }
  `);
  for(let i = 0;i < items.length;i++) {
    let x = <any>parameterListToJSON(items[i]);

    if(x.parameterNames) x.schema.required = x.parameterNames;
    console.log(genMethodEntry(x.className,x.method,x.parameterNames,x.schema));
  }
}

function generateChecks(fileNames: string[],options: ts.CompilerOptions): void {
  let program = ts.createProgram(fileNames,options);
  let output:DecoratedItem[] = [];
  let x = {};

  checker = program.getTypeChecker();
  function isNodeExported(node: ts.Node): boolean {
    return (node.flags & ts.ModifierFlags.Export) !== 0 || (node.parent && node.parent.kind === ts.SyntaxKind.SourceFile);
  }

  function extractDecoratorInfo(node: ts.Node) {
    if(ts.isDecorator(node)) {
      const expr = (<ts.Decorator>node).expression;
      let parentName = "unknown";
      let methodParameters:TypedId[] = [];

      switch(node.parent.kind) {
        case ts.SyntaxKind.FunctionDeclaration:
        {
          const x = (<ts.FunctionDeclaration>(node.parent)).name;

          if(x != null) parentName = x.text;
        }
        break;
        case ts.SyntaxKind.MethodDeclaration:
        {  
          const x = (<ts.FunctionDeclaration>(node.parent)).name;

          if(x != null) parentName = x.text;

          let symbol = checker.getSymbolAtLocation(x);
          let type = checker.getTypeOfSymbolAtLocation(symbol,symbol.valueDeclaration);
          let typeNode = checker.typeToTypeNode(type,null,ts.NodeBuilderFlags.IgnoreErrors|ts.NodeBuilderFlags.WriteTypeParametersInQualifiedName);


          methodParameters = traverseParameterList((<any>typeNode).parameters);

          //console.log(checker.typeToString(checker.getTypeOfSymbolAtLocation(symbol,symbol.valueDeclaration)));
          //console.log(checker.getTypeOfSymbolAtLocation(symbol, symbol.valueDeclaration));
          //console.log(checker.getTypeOfSymbolAtLocation(symbol, symbol.valueDeclaration).parameters);

        }
        break;
        default: throw("unknown decorated type (" + node.parent.kind + ")");
      }

      if(ts.isCallExpression(expr)) {
        const cexpr = <ts.CallExpression>expr;
        const id = <ts.Identifier>cexpr.expression;
        let className = (<any>id.parent.parent.parent.parent).name.text;
        let item:DecoratedItem = { className:className, decorates:parentName, type:id.text, decoratorArgs:[], methodParameters:methodParameters };

        output.push(item);
        for(const arg of cexpr.arguments) {
          switch(arg.kind) {
            case ts.SyntaxKind.StringLiteral:
            {
              let text = tsany.getTextOfNode(arg);
              let s = text.replace(/^["']|["']$/g,'');
              
              item.decoratorArgs.push(s);
            }
            break;
            case ts.SyntaxKind.NumericLiteral:
            {
              item.decoratorArgs.push(parseFloat(tsany.getTextOfNode(arg)));
            }
            break;
            case ts.SyntaxKind.Identifier:
            {
              item.decoratorArgs.push(tsany.getTextOfNode(arg));
            }
            break;
            default: throw("unknown type (" + arg.kind + ") in decorator argument list");
          }
        }
      }
    }
  }

  function visit2(node: ts.Node) {
     let parent = node.parent;

     const intf = <ts.InterfaceDeclaration>parent;
     const x = intf.name;

     if(x != null) {
       switch(node.kind) {
         case ts.SyntaxKind.PropertySignature:
         {
           let parentName = x.text;
           let sig = <ts.PropertySignature>node;
           let name = <any>sig.name;
           let propertyName;
           let symbol = checker.getSymbolAtLocation(name);

           if(name.text) propertyName = name.text;
           if(propertyName && sig.type) {
             let desc = typeToJSON(sig.type);
             let optional = sig.questionToken;

             if(desc) symtab[parentName].members[propertyName] = { desc:desc, optional:optional };
           }
         }
         break;
       }
     }
  }

  function visit(node: ts.Node) {
    if(node.decorators != null) {
       try {
         for(const decorator of node.decorators) extractDecoratorInfo(decorator);
       }
       catch(e) {
         console.log(e);
       }
    }
    else if(isNodeExported(node)) {
      if(node.kind == ts.SyntaxKind.ClassDeclaration) {
        ts.forEachChild(node,visit);
      }
      else if(node.kind == ts.SyntaxKind.InterfaceDeclaration) {
        let name = (<ts.InterfaceDeclaration>node).name;

        if(name != null) {
          let symbol = checker.getSymbolAtLocation(name);
          let comment = ts.displayPartsToString(symbol.getDocumentationComment());

          symtab[name.text] = { members:{}};

          if(comment != null) symtab[name.text].comment = comment;
          ts.forEachChild(node,visit2);
        }
      }
    }
  }

  for(const sourceFile of program.getSourceFiles()) ts.forEachChild(sourceFile,visit);
  fs.writeFileSync("checks.json",JSON.stringify(output,null,2));
  genSource(output);
}

generateChecks(process.argv.slice(2),{ target:ts.ScriptTarget.ES5, module: ts.ModuleKind.CommonJS, experimentalDecorators:true });

