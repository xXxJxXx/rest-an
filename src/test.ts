const check = require('./x');

/**
 * some comment
 */
interface W1test { w?:number|null }

interface W2test { w:string }
interface Xtest { x:string|null }
interface Ytest { y:string }

function GET(p1,p2,p3) {
   return function(target:Object,key:string,descriptor:TypedPropertyDescriptor<any>) {
      let checkObject = check[(<any>target.constructor).name + "." + key];


      if(descriptor === undefined) descriptor = Object.getOwnPropertyDescriptor(target, key);

      let originalMethod = descriptor.value;

      descriptor.value = function () {
        let valid = checkObject.validate(checkObject.argsToSchema(arguments));
        var args = [];

        if(!valid) {
          let err = checkObject.validate.errors[0];

          throw("parameterList" + err.dataPath + " " + err.message);
        }


        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        // note usage of originalMethod here
        var result = originalMethod.apply(this, args);
        return result;
      };
      return descriptor;
   };
}
class test6 {
  /* a comment */
  @GET(1,2,3)
  XXX(w: W1test,x: string,y: number[][]) {
    console.log("howdy");
  }
}

let t:test6 = new test6();
let w1:W1test = {};
let w2:W1test = { w:0 };
let w3:W2test = { w:"0" };
let x1:Xtest = { x:"0" };
let x2:Xtest = { x:null };
let y1:Ytest = { y:"0" };
let y2:Ytest = { y:null };
let z:number[][] = [];

t.XXX(<any>w2,<any>"6",z);
